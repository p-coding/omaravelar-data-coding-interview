WITH cte_airlines as (
SELECT carrier, "name"
FROM public.airlines
where carrier = 'AA' or carrier = 'AS' or carrier = 'DL'
)
select carrier, name 
from cte_airlines